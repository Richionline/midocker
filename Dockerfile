#Version de node de hub.docker.com, el linux virtual que lleva
FROM node:boron

#Crear directorio de la app
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

#Instalar las dependencias indicadas en package.json
COPY package.json /usr/src/app
RUN npm install

#Empaquetar código
COPY . /usr/src/app

#Indico el puerto sobre el que correrá
EXPOSE 8081

#Levanta la app
CMD [ "npm", "start" ]
