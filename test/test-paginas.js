var expect = require('chai').expect; //De la librería chai solo me trae expect, así no me trae todo
var $ = require('chai-jquery');
var request = require('request');

//describe agrupa pruebas
describe('Pruebas sencillas', function() {
  it('Test suma', function() {
    expect(9+4).to.equal(13);
    console.log('Prueba completa');
  })
});

describe('Pruebas de red', function() {
  //Test para validar que responde una web
  it('Test internet', function(done) { //Por convenio se suele poner done y es para validar algo asincrono como un acceso a web
    request.get("http://www.forocoches.com",
      function(error, response, body) {
        expect(response.statusCode).to.equal(200); //Puedes poner más usando ||(or) o &&(and)
        done();
    });
  });

  //Test para validar que responde una web
  it('Test localhost', function(done) { //Por convenio se suele poner done y es para validar algo asincrono como un acceso a web
    request.get("http://localhost:8082",
      function(error, response, body) {
        expect(response.statusCode).to.equal(200); //Puedes poner más usando ||(or) o &&(and)
        done();
        console.log('Prueba completa2');
    });
  });

  //Test para validar que en el body hay etiqueta H1 con cierto contenido
  it('Test H1', function(done) { //Por convenio se suele poner done y es para validar algo asincrono como un acceso a web
    request.get("http://localhost:8082",
      function(error, response, body) {
        expect(body).contains('Bienvenido a mi f... blog');
        //expect(body).should.exist('<h1>\w*</h1>'); //Recordar incluir libreria should de chai ya que si miras arriba sólo hemos incluido expect.
        done();
    });
  });
});

// Relativo a chai-jquery que hemos definido arriba como variable $, las búsquedas que podemos hacer
// $(#identificador) -> Busca en todo el html cualquier id igual al indicado Ej: <xxx id="identificador"...
// $(elemento) -> Busca cualquier etiqueta llamada elemento (Ej: <elemento...)
// $(.clase) -> Buscar un class con nombre clase Ej: <xxx class="clase"...
//              Ejemplo: <table id="prueba" class="clase1"...
//                       $("table#prueba.clase1 tr")


describe('Test contenido HTML', function(done) {
  it('Test H1', function(done) {
    request.get('http://localhost:8082',
              function(error, response, body) {
                expect($('H1')).to.have.text('Bienvenido a mi f... blog');
                done();
              })
  })
});
